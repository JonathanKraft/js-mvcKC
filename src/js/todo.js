export class Todo {
  constructor(label, priority, checked = false){
    this.label = label;
    this.checked = checked;
    this.priority = priority;
  }
  check (){
    this.checked = !this.checked;
    /* if (this.checked){
       this.checked = false;
     }
     else {
       this.checked = true;
    */ }
  }