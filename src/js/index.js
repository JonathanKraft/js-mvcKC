import '../../node_modules/bootstrap/scss/bootstrap.scss';
import '../scss/style.scss';
import {Todo} from './todo';
import {Model} from './model';
import {View} from './view';

let todoInstance = new Todo ("Kiki", 4);
let modelInstance = new Model();
let viewInstance = new View(modelInstance);

modelInstance.addTodo(todoInstance);

console.log(todoInstance);
console.log(modelInstance);


// modelInstance.removeTodo(todoInstance);
// console.log(modelInstance);

viewInstance.initialization();