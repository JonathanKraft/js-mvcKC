import { Todo } from './todo';

export class Model {
  constructor() {
    this.list = [];
  }
  addTodo(newestTodo) {
    if (newestTodo instanceof Todo) {
      this.list.push(newestTodo);
    }
  }
  removeTodo(rmvTodo) {
    for (let i = 0; i < this.list.length; i++) {
      if (rmvTodo.label === this.list[i].label) {
        this.list.splice(i, 1);
      }
    }
  }
}