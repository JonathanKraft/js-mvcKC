import { Model } from './model';
import { Todo } from './todo';

export class View {
  constructor (model = new Model()){
    this.model = model;
  }

  initialization (){
    this.display();
    let form = document.querySelector('#formTodo');
    form.addEventListener('submit', (event) => { // Doesn't alter the "this"
      event.preventDefault();
      let instanceTodo = new Todo (document.querySelector('#label').value, document.querySelector('#priority').value);
      this.model.addTodo(instanceTodo);
      this.display();
    });
  };

  display(){
    let section = document.querySelector('.todolist')
    section.innerHTML = '';
    for (const todo of this.model.list) {
      let p = this.todoHTML(todo);
      section.appendChild(p);
    }
  }

  todoHTML(todo) {
    let p = document.createElement('p');
    p.textContent = todo.label;
    return p;
  }
}